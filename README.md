# go-mysql-rec

#### 介绍
基于Echo框架实现的简单任务记录API

#### 软件架构
基于Echo框架，使用golang开发，使用jwt实现登录验证。后端数据库使用mysql



#### 安装教程

1. 将sql文件导入到数据库中
2. 修改mysql_rec.go文件中的数据库连接配置
3. go run mysql-rec.go

#### 使用说明
**功能描述：**

- 登录

**请求URL：**

- `/tokens`

**请求方式：**

- GET

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|username |是  |string |用户名   |
|password |是  |string |密码   |

**返回示例**

```
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiYWRtaW4iLCJpZCI6MSwiZXhwIjoxNTY3OTI5NTEwfQ.GlUFoUM_13w7cZuy1wcgDa54lEK6CuUkAPbAXGW2MZ0"
}
```

**返回参数说明**

|参数名|类型|说明|
|:-----  |:-----|-----        |
|token |string   |jwt认证token |

***

**功能描述：**

- 获取用户信息

**请求URL：**

- `/users/{userid}`

**请求方式：**

- GET

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|userid |是  |string |id   |

**返回示例**

```
{
    "items": [
        {
            "id": 1,
            "username": "admin",
            "password": "admin111"
        }
    ]
}
```

**返回参数说明**

|参数名|类型|说明|
|:-----  |:-----|-----    |
|id |int   |用户id |
|username |string   |用户名称|
|password |string   |用户密码 |

***

**功能描述：**

- 新建用户

**请求URL：**

- `/users`

**请求方式：**

- PUT

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|username |是  |string |用户名   |
|password |是  |string |密码   |

**返回示例**

```
{
    "created": 5
}
```

**返回参数说明**

|参数名|类型|说明|
|:-----  |:-----|-----    |
|created |int   |用户id |

***

**功能描述：**

- 新建记录

**请求URL：**

- `/records`

**请求方式：**

- PUT

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|name |是  |string |记录名   |
|content |是  |string |记录内容   |

**返回示例**

```
{
    "created": 5
}
```

**返回参数说明**

|参数名|类型|说明|
|:-----  |:-----|-----    |
|created |int   |记录id |

***
**功能描述：**

- 获取全部记录

**请求URL：**

- `/records`

**请求方式：**

- GET

**参数：**
- 无

**返回示例**

```
{
    "items": [
        {
            "id": 6,
            "name": "23321",
            "content": "ALTER TABLE `eap_souke`.`tb_app_comm_problem` ADD COLUMN `app_comm_problem_sort` int(11) NOT NULL COMMENT '排序' AFTER `app_comm_problem_status`;",
            "createTime": "2019-08-02 14:38:20"
        },
        {
            "id": 2,
            "name": "test2",
            "content": "",
            "createTime": "0000-00-00 00:00:00"
        },
        {
            "id": 3,
            "name": "t22est2",
            "content": "",
            "createTime": "2019-08-02 13:38:23"
        },
        {
            "id": 4,
            "name": "t22est2",
            "content": "select * from 433234 where id 04959405",
            "createTime": "2019-08-02 13:46:11"
        },
        {
            "id": 5,
            "name": "t22estww2",
            "content": "select * from 433fddfff4 where id 04959405",
            "createTime": "2019-08-02 13:58:24"
        },
        {
            "id": 7,
            "name": "test1",
            "content": "23231",
            "createTime": "2019-09-05 16:37:18"
        }
    ]
}
```

**返回参数说明**

|参数名|类型|说明|
|:-----  |:-----|-----    |
|id |int   |记录id |
|name |string   |记录名称 |
|content |string   |记录内容 |
|createTime |string   |创建时间 |

***

**功能描述：**

- 删除记录

**请求URL：**

- `/records/{id}`

**请求方式：**

- DELETE

**参数：**

|参数名|必选|类型|说明|
|:----    |:---|:----- |-----   |
|id |是  |string |记录id   |

**返回示例**

```
{
    "deleted": 1
}
```

**返回参数说明**

|参数名|类型|说明|
|:-----  |:-----|-----    |
|deleted |int   |删除记录数量 |
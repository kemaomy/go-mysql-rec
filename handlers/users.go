package handlers

import (
	"go-mysql-rec/models"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

//PutUser add user
func (h *Handler) PutUser() echo.HandlerFunc {
	return func(c echo.Context) error {
		var user models.User

		c.Bind(&user)

		id, err := models.PutUser(h.DB, user.UserName, user.PassWord)
		if err == nil {
			return c.JSON(http.StatusCreated, H{
				"created": id,
			})
		}
		return err

	}
}

//GetUser get usr by userid
func (h *Handler) GetUser() echo.HandlerFunc {
	return func(c echo.Context) error {
		var user models.User
		claims := c.Get("user").(*jwt.Token).Claims.(*JwtCustomClaims)

		c.Bind(&user)
		result, err := models.GetUser(h.DB, claims.ID)
		if err == nil {
			return c.JSON(http.StatusOK, result)
		}
		return err
	}
}

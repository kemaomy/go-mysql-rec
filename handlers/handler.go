package handlers

import (
	"database/sql"

	"github.com/dgrijalva/jwt-go"
)

//H data return format
type H map[string]interface{}

//Handler extend mysql db
type (
	Handler struct {
		DB *sql.DB
	}
)

// JwtCustomClaims are custom claims extending default ones.
type JwtCustomClaims struct {
	Name string `json:"name"`
	ID   int64  `json:"id"`
	jwt.StandardClaims
}

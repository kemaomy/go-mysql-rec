package main

import (
	"database/sql"
	"go-mysql-rec/handlers"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	db := connDB()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Configure middleware with the custom claims type
	config := middleware.JWTConfig{
		Claims:     &handlers.JwtCustomClaims{},
		SigningKey: []byte("secret"),
		Skipper: func(c echo.Context) bool {
			if c.Path() == "/tokens" {
				return true
			}
			return false
		},
	}
	e.Use(middleware.JWTWithConfig(config))

	h := &handlers.Handler{DB: db}

	e.GET("/records", h.GetRecords())
	e.PUT("/records", h.PutRecord())
	e.DELETE("/records/:id", h.DeleteRecord())

	e.PUT("/users", h.PutUser())
	e.GET("/users/:id", h.GetUser())
	e.GET("/tokens", h.GetToken())

	e.Logger.Fatal(e.Start(":8000"))
}

func connDB() *sql.DB {
	db, err := sql.Open("mysql", "root:root@/mysql_rec?charset=utf8")
	if err != nil {
		panic(err)
	}
	return db
}
